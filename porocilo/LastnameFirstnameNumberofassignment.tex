% ============================================================================================
% This is a LaTeX template used for the course
%
%  I M A G E   B A S E D   B I O M E T R I C S
%
% Faculty of Computer and Information Science
% University of Ljubljana
% Slovenia, EU
%
% You can use this template for whatever reason you like.
% If you have any questions feel free to contact
% ziga.emersic@fri.uni-lj.si
% ============================================================================================

\documentclass[9pt]{IEEEtran}

% basic
\usepackage[english]{babel}
\usepackage{graphicx,epstopdf,fancyhdr,amsmath,amsthm,amssymb,url,array,textcomp,svg,listings,hyperref,xcolor,colortbl,float,gensymb,longtable,supertabular,multicol,placeins}

 % `sumniki' in names
\usepackage[utf8x]{inputenc}

 % search and copy for `sumniki'
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{caption}
\input{glyphtounicode}
\pdfgentounicode=1

% tidy figures
\graphicspath{{./figures/}}
\DeclareGraphicsExtensions{.pdf,.png,.jpg,.eps}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor trig-gs}

% ============================================================================================

\title{\vspace{0ex} %
% TITLE IN HERE:
Plant recognition
\\ \large{Assignment \#3}\\ \normalsize{Image Based Biometrics 2019/20, Faculty of Computer and Information Science, University of Ljubljana}}
\author{ %
% AUTHOR IN HERE:
Tadej Vatovani and Jovan Toroman and Domen Požrl 
\vspace{-4.0ex}
}

% ============================================================================================

\begin{document}

\maketitle

\begin{abstract}
We describe the development of a mobile application that uses a pre-trained model to classify pictures of household plants. We compare the performances of different convolutional neural network architectures and embed the best performing model into a mobile application that uses the smartphone's camera to capture a picture of a household plant and the pre-trained model to classify it. 
\end{abstract}

\section{Introduction}
Most households contain at least a few plants. Many of us (excluding amateur and professional florists) usually do not know the names of these plants. In this paper, we develop a mobile application that helps us capture a picture of a household plant and then use that picture to figure out the name of the plant. We use convolutional neural networks to perform the classification. We present a few different architectures and their performance on the collected database of 20 different household plants. Each plant is presented with 30 different pictures totalling to 600 pictures of plants. 

\section{Methodology}
In this section we briefly describe the different architectures of the selected networks.

\subsection{MobileNet}
MobileNet \cite{mobilenet} was developed with a goal of reducing the size of the model and making it functional in environments with limited resources such as smart phones and embedded systems, only sacrificing a small portion of accuracy in return. The main idea behind the MobileNet architecture is to replace standard convolution with depthwise separable convolution, which is a combination of depthwise and pointwise convolution. The main difference is, that the standard convolution combines all the channels of an image while the depthwise only joins the values of the same channels. Pointwise convolution is then used to combine all the channels together. We get a similar result as with the standard convolution, but we use up less resources which is desirable when deploying on a device with limited resources.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\columnwidth]{mobilenet.jpg}
    \caption{MobileNet structure}
    \label{fig:mobilenetarchitecture}
\end{figure}

\subsection{SqueezeNet}
SqueezeNet~\cite{squeezenet} was created with network size in mind. Authors aimed to reduce the network size for a given accuracy requirement. They achieved accuracy level similar to that of AlexNet~\cite{alexnet}, with network having fifty times less parameters and taking up five hundred ten  times less space. It was also one of the first efficient implementations of network training on GPU. The authors registered it for ILSVRC-2012 competition, where it achieved top-5 test error rate of 15.3\%,
compared to 26.2\% achieved by the second-best entry. The neural network consists of five convolutional layers, where some of them are followed by max-pooling layers, and three fully-connected layers with a final thousand-way softmax. It has sixty million parameters and six hundred fifty thousand neurons. Due to its smaller number of parameters, this network takes less time to train. Its small model size makes it a good fit for deployment on mobile devices.

\subsection{ShuffleNet}
Shuffle~\cite{shufflenet} was designed for mobile devices and as such is aimed at device with limited computing power. They manged to speedup the network and achieve similiar accuracy to AlexNet~\cite{alexnet} while being 13 times as fast. The main technique that was implemented was the channel shuffle operation. With the channel shuffle they aimed to enable stacked group convoloutions to cross talk between them by allowing them to obtain input data from different groups. By taking advantage of the channel shuffle ooperation, they proposed a ShuffleNet unit \ref{fig:shufflenetunit}. The proposed network is then mainly composed of a stack of ShuffleNet units grouped into three stage.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\columnwidth]{shuffleunit.jpg}
    \caption{A representation of a ShuffleNet unit.}
    \label{fig:shufflenetunit}
\end{figure}

\section{Results}
\subsection{Model evaluation}
We evaluated multiple models, which have been shown to perform well at image classification tasks.
\subsubsection{MobileNet}
For training the MobileNet we used a Python and Keras implementation \cite{mobilenetgit, MobileNetv2}, but before training we had to resize all the images to the size of 224$\times$224. We split the data into a training and a validation set, using 85\% of the images for training and 15\% for validation. After 300 epochs of training, the accuracy of the model was almost 64\% as can be seen in Figure \ref{fig:mobilenetacc}. This is significantly better than using the random classifier which has an accuracy of 5\%.

\begin{figure}[h]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=1\columnwidth]{mobilenetacc.png}
    \caption{The accuracy of MobileNet with regards to the number of epochs.}
    \label{fig:mobilenetacc}
\end{figure}

\subsubsection{SqueezeNet}
SqueezeNet was trained with the train data and evaluated on test data. The train/test split was made with eighty-five percent train data. Results varied with regard to number of training epochs. They can be seen in Table 1.

\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Number of epochs & 2   & 10  & 50   & 100  & 150  \\ \hline
Accuracy         & 4\% & 7\% & 17\% & 26\% & 27\% \\ \hline
\end{tabular}
\caption{Accuracy with regard to number of epochs}
\label{tab:table1}
\end{table}

For twenty existing classes, random case accuracy is five percent. We can see that SqueezeNet achieves more than five times higher accuracy compared to random case. This accuracy could still be improved with adding more training data or training the model through more epochs. We did not test accuracy for this network on three hundred epochs, since the results for hundred and fifty epochs were only slightly better than the ones for hundred epochs.


\subsubsection{ShuffleNet}
ShuffleNet was trained with the train data and evaluated on test data. The train/test split was made with seventy-five percent train data. Results varied with regard to number of training epochs. They can be seen in Table 2. \ref{fig:table2}

\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Number of epochs & 2   & 10  & 50   & 100  & 200  \\ \hline
Accuracy         & 4\% & 5\% & 25\% & 30\% & 31\% \\ \hline
\end{tabular}
\caption{Accuracy with regard to number of epochs}
\label{tab:table2}
\end{table}

This accuracy could still be improved with adding more training data or training the model through more epochs. Another improvement we could see is the learning rate, since we only used SGD together with a linear reduction in learning rate. We did not test accuracy for this network on three hundred epochs, since the results for two-hundred epochs were only slightly better than the ones for hundred epochs. 


We believe that the reason for significant differences in accuracy between MobileNet and other two approaches lies in the fact that MobileNet implementation are more accurate and reliable since it is a very popular architecture.


\section{Share of Work}

\subsection{Domen Požrl}
I gathered and labeled half of the data and evaluated the MobileNet and also wrote the parts of the report about this topic.

\subsection{Tadej Vatovani}
Worked on most of the flutter app, this involves creating the screens, enabling the camera and image import. I also implemented the local neural network on the mobile device by converting the MobileNet trained model into a tensorflow lite model. I evaluated the ShuffleNet and wrote it's Methodology and Results sections.

\subsection{Jovan Toroman}
I gathered and labeled half of the data, evaluated one machine learning model, and helped with operational tasks like model file conversion, helping debug flutter app, evaluating potential libraries for Java-Python interop, and others. I created a database client for persisting plant data and experimented with Python web server for evaluating photos. I also wrote SqueezeNet subsections of Methodology and Results sections, as well as a part of the conclusion.


 \section{Conclusion}

We achieve satisfactory results using MobileNet model trained through three hundred epochs. Application is able to detect the correct plant in more than sixty percent of the cases.

\bibliographystyle{IEEEtran}
\bibliography{bibliography}

\end{document}