package com.example.plant_app

import java.time.Instant

class User {

    var id: Int? = null
    var username: String? = null
    var userBio: String? = null
    var passwordHash: String? = null
    var email: String? = null
    var datumUstvarjanja: Instant? = null

    constructor() {}

    constructor(id: Int?, username: String, userBio: String, passwordHash: String, email: String, datumUstvarjanja: Instant) {
        this.id = id
        this.username = username
        this.userBio = userBio
        this.passwordHash = passwordHash
        this.email = email
        this.datumUstvarjanja = datumUstvarjanja
    }
}
