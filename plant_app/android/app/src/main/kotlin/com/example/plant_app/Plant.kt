package com.example.plant_app

import android.net.Uri

import java.time.Instant
import java.util.Date

class Plant {
    var id: Int? = null
    var slovenskoIme: String? = null
    var latinskoIme: String? = null
    var domovina: String? = null
    var oskrba: String? = null
    var prezimovanje: String? = null
    var obrezovanje: String? = null
    var razmnozevanje: String? = null
    var presajanje: String? = null
    var dognojevanje: String? = null
    var varstvo: String? = null
    var najpogostejseNapake: String? = null
    var uriSlike: Uri? = null
    var datumUstvarjanja: Instant? = null
    var userId: Int? = null

    constructor() {}

    constructor(slovenskoIme: String, latinskoIme: String, domovina: String, oskrba: String,
                prezimovanje: String, obrezovanje: String, razmnozevanje: String, presajanje: String,
                dognojevanje: String, varstvo: String, najpogostejseNapake: String, uriSlike: Uri,
                datumUstvarjanja: Instant, id: Int?, userId: Int?) {
        this.slovenskoIme = slovenskoIme
        this.latinskoIme = latinskoIme
        this.domovina = domovina
        this.oskrba = oskrba
        this.prezimovanje = prezimovanje
        this.obrezovanje = obrezovanje
        this.razmnozevanje = razmnozevanje
        this.presajanje = presajanje
        this.dognojevanje = dognojevanje
        this.varstvo = varstvo
        this.najpogostejseNapake = najpogostejseNapake
        this.uriSlike = uriSlike
        this.datumUstvarjanja = datumUstvarjanja
        this.id = id
        this.userId = userId
    }
}
