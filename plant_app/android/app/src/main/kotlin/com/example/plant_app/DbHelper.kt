package com.example.plant_app

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.net.Uri
import android.os.Build

import androidx.annotation.RequiresApi

import java.time.Instant
import java.util.ArrayList

class DbHelper(private val context: MainActivity) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {

    val users: List<User>
        @RequiresApi(api = Build.VERSION_CODES.O)
        get() {
            val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
            val c = db.rawQuery("SELECT * FROM $USERS_TABLE_NAME;", null)
            val ret = ArrayList<User>()
            while (c.moveToNext()) {
                val u = User()
                u.id = c.getInt(0)
                u.username = c.getString(1)
                u.userBio = c.getString(2)
                u.passwordHash = c.getString(3)
                u.email = c.getString(4)
                u.datumUstvarjanja = Instant.parse(c.getString(5))
                ret.add(u)
            }
            db.close()
            return ret
        }

    init {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("CREATE TABLE IF NOT EXISTS " + DATA_TABLE_NAME + " (_id integer PRIMARY KEY AUTOINCREMENT, "
                + "user_id integer,"
                + "slovensko_ime text,"
                + "latinsko_ime text,"
                + "domovina text,"
                + "oskrba text,"
                + "prezimovanje text,"
                + "obrezovanje text,"
                + "razmnozevanje text,"
                + "presajanje text,"
                + "dognojevanje text,"
                + "varstvo text,"
                + "najpogostejse_napake text,"
                + "uri_slike text,"
                + "datum_ustvarjanja DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + "FOREIGN KEY (user_id) REFERENCES users(user_id)"
                + ");")

        db.execSQL("CREATE TABLE IF NOT EXISTS " + USERS_TABLE_NAME + " (_id integer PRIMARY KEY AUTOINCREMENT, "
                + "username,"
                + "user_bio,"
                + "password_hash,"
                + "email,"
                + "datum_ustvarjanja DATETIME DEFAULT CURRENT_TIMESTAMP"
                + ");")
        db.close()
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun getPlants(userId: Int): List<Plant> {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        val c = db.rawQuery("SELECT * FROM $DATA_TABLE_NAME WHERE user_id=$userId;", null)
        val ret = ArrayList<Plant>()
        while (c.moveToNext()) {
            val p = Plant()
            p.id = c.getInt(0)
            p.userId = c.getInt(1)
            p.slovenskoIme = c.getString(2)
            p.latinskoIme = c.getString(3)
            p.domovina = c.getString(4)
            p.oskrba = c.getString(5)
            p.prezimovanje = c.getString(6)
            p.obrezovanje = c.getString(7)
            p.razmnozevanje = c.getString(8)
            p.presajanje = c.getString(9)
            p.dognojevanje = c.getString(10)
            p.varstvo = c.getString(11)
            p.najpogostejseNapake = c.getString(12)
            p.uriSlike = Uri.Builder().appendPath(c.getString(13)).build()
            p.datumUstvarjanja = Instant.parse(c.getString(14))
            ret.add(p)
        }
        db.close()
        return ret
    }

    fun addPlant(p: Plant) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("INSERT INTO " + DATA_TABLE_NAME + " (user_id,slovensko_ime,latinsko_ime,domovina," +
                "oskrba,prezimovanje,obrezovanje,razmnozevanje,presajanje,dognojevanje,varstvo," +
                "najpogostejse_napake,uri_slike) VALUES (" + p.userId + "," + p.slovenskoIme +
                "," + p.latinskoIme + "," + p.domovina + "," + p.oskrba + "," +
                p.prezimovanje + "," + p.obrezovanje + "," + p.razmnozevanje + "," +
                p.presajanje + "," + p.dognojevanje + "," + p.varstvo + "," +
                p.najpogostejseNapake + "," + p.uriSlike + ");")
        db.close()
    }

    fun updatePlant(p: Plant) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("UPDATE " + DATA_TABLE_NAME + " SET user_id=" + p.userId + ",slovensko_ime=" +
                p.slovenskoIme + ",latinsko_ime=" + p.latinskoIme + ",domovina=" + p.domovina +
                "oskrba=" + p.oskrba + ",prezimovanje=" + p.prezimovanje + ",obrezovanje=" +
                p.obrezovanje + ",razmnozevanje=" + p.razmnozevanje + ",presajanje=" +
                p.presajanje + ",dognojevanje=" + p.dognojevanje + ",varstvo=" + p.varstvo +
                ",najpogostejse_napake=" + p.najpogostejseNapake + ",uri_slike=" + p.uriSlike +
                " WHERE _id=" + p.id + ";")
        db.close()
    }

    fun deletePlant(p: Plant) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("DELETE FROM " + DATA_TABLE_NAME + " WHERE _id=" + p.id + ";")
    }

    fun addUser(u: User) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("INSERT INTO " + USERS_TABLE_NAME + " (username, user_bio, password_hash, email) " +
                "VALUES (" + u.username + "," + u.userBio +
                "," + u.passwordHash + "," + u.email + ");")
        db.close()
    }

    fun updateUser(u: User) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("UPDATE " + USERS_TABLE_NAME + " SET username=" + u.username + ",user_bio=" +
                u.userBio + ",password_hash=" + u.passwordHash + ",email=" + u.email +
                " WHERE _id=" + u.id + ";")
        db.close()
    }

    fun deleteUser(u: User) {
        val db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        db.execSQL("DELETE FROM " + USERS_TABLE_NAME + " WHERE _id=" + u.id + ";")
    }

    override fun onCreate(db: SQLiteDatabase) {}

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    companion object {

        private val DATABASE_NAME = "plant_db"
        private val DATA_TABLE_NAME = "plants"
        private val USERS_TABLE_NAME = "users"
    }
}
