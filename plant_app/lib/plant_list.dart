import 'package:flutter/material.dart';
import 'package:plant_app/plant_card.dart';
import 'package:plant_app/plant_model.dart';

class ListOfPlants extends StatelessWidget {
  final List<Plant> plants;

  ListOfPlants(this.plants);

  ListView buildList(context) {
    return new ListView.builder(
      itemCount: plants.length,
      itemBuilder: (context, int) {
        return PlantCard(plants[int]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildList(context);
  }
}