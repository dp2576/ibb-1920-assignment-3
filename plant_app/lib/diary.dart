library diary;

import 'package:flutter/material.dart';

// A screen that allows users to take a picture using a given camera.
class DiaryScreen extends StatefulWidget {
  DiaryScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  DiaryScreenState createState() => DiaryScreenState();
}

class DiaryScreenState extends State<DiaryScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  static const TextStyle textStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Colors.white);

  static const TextStyle titleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "Roboto");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Theme.of(context).accentColor,
          title: Row(children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
                child: Icon(
                  Icons.import_contacts,
                  size: 28,
                )),
            Text(
              widget.title,
              style: titleStyle,
            ),
          ]),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/cactus-background.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.photo_library),
                      iconSize: 80,
                      tooltip: 'Switch to photographs',
                      onPressed: () {},
                    ),
                    Text(
                      'FOTOGRAFIJE',
                      style: textStyle,
                    ),
                  ]),
                  Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.event_note),
                      iconSize: 80,
                      tooltip: 'Switch to calendar',
                      onPressed: () {},
                    ),
                    Text(
                      'KOLEDAR',
                      style: textStyle,
                    ),
                  ]),
                ],
              ),
            )
          ],
        ));
  }
}
