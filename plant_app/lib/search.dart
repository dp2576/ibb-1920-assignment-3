library search;

import 'package:flutter/material.dart';
import 'package:plant_app/custom_icons_icons.dart';
import 'package:plant_app/plants_blooming.dart';
import 'package:plant_app/plants_leafy.dart';

// A screen that allows users to take a picture using a given camera.
class SearchScreen extends StatefulWidget {
  SearchScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  static const TextStyle textStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Colors.white);

  static const TextStyle titleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "Roboto");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Theme.of(context).accentColor,
          title: Row(children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
                child: Icon(
                  Icons.search,
                  size: 28,
                )),
            Text(
              widget.title,
              style: titleStyle,
            ),
          ]),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/leaf-background2.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.local_florist),
                      iconSize: 80,
                      tooltip: 'Switch to blooming plants',
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => BloomingPlantsScreen(
                              title: "CVETOČE RASTLINE",
                            ),
                          ),
                        );
                      },
                    ),
                    Text(
                      'CVETOČE',
                      style: textStyle,
                    ),
                  ]),
                  Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: Icon(CustomIcons.leaf),
                      iconSize: 80,
                      tooltip: 'Switch to leaf plants',
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                LeafyPlantsScreen(title: "LISTNATE RASTLINE"),
                          ),
                        );
                      },
                    ),
                    Text(
                      'LISTNATE',
                      style: textStyle,
                    ),
                  ]),
                ],
              ),
            )
          ],
        ));
  }
}
