// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plant_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlantList _$PlantListFromJson(Map<String, dynamic> json) {
  return PlantList(
    plants: (json['plants'] as List)
        ?.map(
            (e) => e == null ? null : Plant.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PlantListToJson(PlantList instance) => <String, dynamic>{
      'plants': instance.plants,
    };

Plant _$PlantFromJson(Map<String, dynamic> json) {
  return Plant(
    json['imeSLO'] as String,
    json['imeLATIN'] as String,
    domovina: json['domovina'] as String,
    oskrba: json['oskrba'] as String,
    prezimovanje: json['prezimovanje'] as String,
    obrezovanje: json['obrezovanje'] as String,
    razmnozevanje: json['razmnozevanje'] as String,
    presajanje: json['presajanje'] as String,
    dognojevanje: json['dognojevanje'] as String,
    varstvo: json['varstvo'] as String,
    napake: json['napake'] as String,
  )..imagePath = json['imagePath'] as String;
}

Map<String, dynamic> _$PlantToJson(Plant instance) => <String, dynamic>{
      'imeSLO': instance.imeSLO,
      'imeLATIN': instance.imeLATIN,
      'domovina': instance.domovina,
      'oskrba': instance.oskrba,
      'prezimovanje': instance.prezimovanje,
      'obrezovanje': instance.obrezovanje,
      'razmnozevanje': instance.razmnozevanje,
      'presajanje': instance.presajanje,
      'dognojevanje': instance.dognojevanje,
      'varstvo': instance.varstvo,
      'napake': instance.napake,
      'imagePath': instance.imagePath,
    };
