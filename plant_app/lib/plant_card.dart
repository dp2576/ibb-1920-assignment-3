import 'package:flutter/material.dart';
import 'package:plant_app/plant_details.dart';
import 'package:plant_app/plant_model.dart';

class PlantCard extends StatefulWidget {
  final Plant plant;

  PlantCard(this.plant);

  @override
  PlantCardState createState() {
    return PlantCardState(plant);
  }
}

class PlantCardState extends State<PlantCard> {
  Plant plant;
  String renderUrl;

  PlantCardState(this.plant);

  void initState() {
    super.initState();
    renderPlantPic();
  }

  void renderPlantPic() {
    //await plant.getImageUrl();
    setState(() {
      renderUrl =
          plant.imagePath == null ? "assets/iamges/plants/monstera.png" : plant.imagePath;
    });
  }

  static const TextStyle labelStyle = TextStyle(
    fontSize: 30,
    fontWeight: FontWeight.bold,
    color: const Color.fromRGBO(35, 35, 35, 1),
  );

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: () => showPlantDetailPage(),
      child: new Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Card(
          elevation: 2,
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                  child: Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          new BoxShadow(
                              color: Colors.black12,
                              offset: new Offset(0.0, 8.0),
                              spreadRadius: 3,
                              blurRadius: 10)
                        ],
                        image: new DecorationImage(
                          image: AssetImage(plant.imagePath == null
                              ? "assets/images/plants/placeholder.png"
                              : plant.imagePath),
                          fit: BoxFit.cover,
                        )),
                  )),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
                      child: Icon(
                        Icons.info,
                        size: 28,
                        color: new Color.fromRGBO(35, 35, 35, 1),
                      )),
                  Text(
                    plant.imeSLO,
                    style: labelStyle,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  showPlantDetailPage() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new PlantDetailsScreen(plant: plant);
    }));
  }
}
