library profile;

import 'package:flutter/material.dart';

// A screen that allows users to take a picture using a given camera.
class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  static const TextStyle labelStyle = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Color.fromRGBO(245, 193, 72, 1));
      
  static const TextStyle textStyle = TextStyle(
      fontSize: 19,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      color: Colors.white);

  static const TextStyle settingsTextStyle = TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      color: Colors.white);

  static const TextStyle titleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "Roboto");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: new AppBar(
        backgroundColor: Theme.of(context).accentColor,
        title: Row(children: <Widget>[
          Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
              child: Icon(
                Icons.person,
                size: 28,
              )),
          Text(
            widget.title,
            style: titleStyle,
          ),
        ]),
      ),
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 35.0, 0, 0),
                    child: Container(
                        height: 200.0,
                        width: 200.0,
                        child: Container(
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              boxShadow: [
                                new BoxShadow(
                                    color: Colors.black12,
                                    offset: new Offset(0.0, 8.0),
                                    spreadRadius: 3,
                                    blurRadius: 10)
                              ],
                              image: new DecorationImage(
                                image: AssetImage("assets/images/profile.png"),
                                fit: BoxFit.cover,
                              )),
                        ))),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 57.0, 0, 0),
                    child: ClipPath(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Container(
                        width: double.infinity,
                        color: new Color.fromRGBO(35, 35, 35, 1),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 120, 20, 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(
                                "UPORABNIŠKO IME",
                                style: labelStyle,
                              ),
                              Padding(
                                  padding:
                                      EdgeInsets.fromLTRB(0, 10.0, 0, 20.0),
                                  child: Text(
                                    "Neža Medved",
                                    style: textStyle,
                                  )),
                              Text(
                                "EMAIL",
                                style: labelStyle,
                              ),
                              Padding(
                                  padding:
                                      EdgeInsets.fromLTRB(0, 10.0, 0, 60.0),
                                  child: Text(
                                    "prikazni.email@email.com",
                                    style: textStyle,
                                  )),
                              Row(children: <Widget>[
                                Icon(
                                  Icons.build,
                                  color: Theme.of(context).accentColor,
                                  size: 16,
                                ),
                                Text(
                                  " NASTAVITVE",
                                  style: labelStyle,
                                ),
                              ]),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Spremenite profilno sliko",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Spremenite uporabniško ime",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Spremenite e-poštni naslov",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Spremenite geslo",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Izpiši se iz profila",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Opomnik za obvestila",
                                    style: settingsTextStyle,
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                                  child: Text(
                                    "Dostop do galerije",
                                    style: settingsTextStyle,
                                  )),
                            ],
                          ),
                        ),
                      ),
                      clipper: TopClipper(),
                    )),
              ],
            ),
          )),
    );
  }
}

class TopClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    double height = 80.0;
    path.moveTo(0.0, height);
    path.quadraticBezierTo(size.width / 2, 0, size.width, height);
    path.lineTo(size.width, height);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
