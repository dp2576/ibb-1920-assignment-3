import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:plant_app/image_crop.dart';
import 'package:plant_app/main.dart';
import 'camera.dart';

// A screen that allows users to take a picture using a given camera.
class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void switchToCamera() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => TakePictureScreen()));
  }

  void pickImage() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ImageCropScreen(image: imageFile),
      ),
    );
  }

  static const TextStyle textStyle = TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Colors.white);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/login-background.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("VPIS", style: textStyle),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Container(
                    width: 300,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white38,
                      borderRadius: BorderRadius.circular(45),
                    ),
                    child: TextField(
                        obscureText: false,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "UPORABNIŠKO IME",
                            
                            contentPadding: EdgeInsets.all(10))),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: Container(
                    width: 300,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white38,
                      borderRadius: BorderRadius.circular(45),
                    ),
                    child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "GESLO",
                            contentPadding: EdgeInsets.all(10))),
                  )),
              Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: IconButton(
                    color: Colors.black,
                    icon: Icon(Icons.arrow_forward),
                    iconSize: 25,
                    tooltip: 'Login',
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MainScreen(title: "Plants"),
                        ),
                      );
                    },
                  ))
            ],
          ),
        )
      ],
    ));
  }
}
