import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:plant_app/image_crop.dart';
import 'camera.dart';

// A screen that allows users to take a picture using a given camera.
class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void switchToCamera() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => TakePictureScreen()));
  }

  void pickImage() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ImageCropScreen(image: imageFile),
      ),
    );
  }

  static const TextStyle textStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Colors.white);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/leaf-background.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add_a_photo),
                  iconSize: 80,
                  tooltip: 'Switch to camera',
                  onPressed: () {
                    switchToCamera();
                  },
                ),
                Text('POSNEMI', style: textStyle),
              ]),
              Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add_photo_alternate),
                  iconSize: 80,
                  tooltip: 'Switch to gallery',
                  onPressed: () {
                    pickImage();
                  },
                ),
                Text('UVOZI', style: textStyle),
              ]),
            ],
          ),
        )
      ],
    );
  }
}
