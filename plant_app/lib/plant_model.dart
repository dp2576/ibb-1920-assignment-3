import 'package:json_annotation/json_annotation.dart';
part 'plant_model.g.dart';

const List PLANT_NAMES = [
  'Anthurium scherzerianum',
  'Aphelandra squarrosa',
  'Beaucarnea recurvata',
  'Begonia rex',
  'Chlorophytum comosum',
  'Clivia miniata',
  'Codiaeum variegatum',
  'Cyclamen persicum',
  'Cymbidium hybridi',
  'Dracaena  deremensis',
  'Dracaena marginata',
  'Euphorbia pulcherrima',
  'Ficus benjamini',
  'Gerbera hybrid',
  'Kalanchoe blossfeldiana',
  'Monstera deliciosa',
  'Rhododendron simsii',
  'Sansevieria cylindrica',
  'Schlumbergera hybrid',
  'Zantedeschia aethiopica'
];

@JsonSerializable()
class PlantList {
  List<Plant> plants;

  PlantList({this.plants});

  factory PlantList.fromJson(List<dynamic> json) {
    return PlantList(
        plants: json
            .map((e) => Plant.fromJson(e as Map<String, dynamic>))
            .toList());
  }
  List<Plant> bloomingPlants() {
    return plants.sublist(11);
  }

  List<Plant> leafPlants() {
    return plants.sublist(0, 11);
  }

  void sortByLatinName() {
    plants.sort((a, b) {
      return a.imeLATIN
          .toString()
          .toLowerCase()
          .compareTo(b.imeLATIN.toString().toLowerCase());
    });
  }
}

@JsonSerializable()
class Plant {
  final String imeSLO;
  final String imeLATIN;
  String domovina = '';
  String oskrba = '';
  String prezimovanje = '';
  String obrezovanje = '';
  String razmnozevanje = '';
  String presajanje = '';
  String dognojevanje = '';
  String varstvo = '';
  String napake = '';
  String imagePath = "assets/images/plants/placeholder.png";

  Plant(this.imeSLO, this.imeLATIN,
      {this.domovina,
      this.oskrba,
      this.prezimovanje,
      this.obrezovanje,
      this.razmnozevanje,
      this.presajanje,
      this.dognojevanje,
      this.varstvo,
      this.napake});
  factory Plant.fromJson(Map<String, dynamic> json) => _$PlantFromJson(json);

  Map<String, dynamic> toJson() => _$PlantToJson(this);
}
