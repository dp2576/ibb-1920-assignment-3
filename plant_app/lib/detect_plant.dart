library profile;

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:plant_app/main.dart';
import 'package:plant_app/myplants.dart';
import 'package:plant_app/plant_details.dart';
import 'package:plant_app/plant_model.dart';
import 'package:tflite/tflite.dart';

// A screen that allows users to take a picture using a given camera.
class DetectPlantScreen extends StatefulWidget {
  DetectPlantScreen({Key key, this.image}) : super(key: key);
  final File image;
  @override
  DetectPlantScreenState createState() => DetectPlantScreenState();
}

class DetectPlantScreenState extends State<DetectPlantScreen> {
  Plant detectedPlant;
  int selectedIndex = 1;
  bool isReady = false;
  bool noPlant = false;
  @override
  void initState() {
    super.initState();
    detectPlant();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void detectPlant() async {
    String res = await Tflite.loadModel(
        model: "assets/tf_lite_model.tflite",
        labels: "assets/labels.txt",
        numThreads: 1 // defaults to 1
        );

    var recognitions = await Tflite.runModelOnImage(
        path: widget.image.path, // required
        numResults: 3,
        threshold: 0.3,
        imageMean: 0.0,
        imageStd: 255.0,
        asynch: true);
    var recognitionsList = recognitions.map((re) => re["index"]).toList();

    if (recognitionsList.isEmpty) {
      setState(() {
        noPlant = true;
      });
    } else {
      String plantsJson =
          await rootBundle.loadString("assets/opisi-rastlin.json");
      var plantsMap = json.decode(plantsJson);
      PlantList plants = new PlantList.fromJson(plantsMap);
      plants.sortByLatinName();
      detectedPlant = plants.plants[recognitionsList[0]];
    }
    setState(() {
      isReady = true;
    });
    await Tflite.close();
  }

  static const TextStyle labelStyle = TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Color.fromRGBO(245, 193, 72, 1));

  void onItemTapped(index) {
    if (noPlant) {
      selectedIndex = index;
      switch (index) {
        case 0:
          Navigator.pop(context);
          break;
        case 1:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MainScreen(),
            ),
          );
          break;
      }
    } else {
      selectedIndex = index;
      switch (index) {
        case 0:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PlantDetailsScreen(plant: detectedPlant),
            ),
          );
          break;
        case 1:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MainScreen(),
            ),
          );
          break;
        case 2:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MyPlantsScreen(title: "MOJE RASTLINE"),
            ),
          );
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: !isReady
            ? Center(child: CircularProgressIndicator())
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: (noPlant
                    ? <Widget>[
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 35.0, 0, 20.0),
                            child: Text("Na sliki ni zaznane rastline",
                                style: labelStyle,
                                textAlign: TextAlign.center)),
                      ]
                    : <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 35.0, 0, 20.0),
                          child: Container(
                            height: 233.0,
                            width: 233.0,
                            child: Container(
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    new BoxShadow(
                                        color: Colors.black12,
                                        offset: new Offset(0.0, 8.0),
                                        spreadRadius: 3,
                                        blurRadius: 10)
                                  ],
                                  image: new DecorationImage(
                                    image: FileImage(widget.image),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                          ),
                        ),
                        Text(detectedPlant.imeSLO.toUpperCase(),
                            style: labelStyle, textAlign: TextAlign.center)
                      ])),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: (noPlant
            ? <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.arrow_back),
                  title: Text(""),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text(""),
                ),
              ]
            : <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.info),
                  title: Text(""),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text(""),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.favorite),
                  title: Text(""),
                ),
              ]),
        currentIndex: noPlant ? 0 : selectedIndex,
        onTap: onItemTapped,
      ),
    );
  }
}
