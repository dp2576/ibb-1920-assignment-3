import 'package:flutter/material.dart';
import 'package:plant_app/home.dart';
import 'package:plant_app/diary.dart';
import 'package:plant_app/login.dart';
import 'package:plant_app/myplants.dart';
import 'package:plant_app/search.dart';
import 'package:plant_app/profile.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Plants',
      theme: new ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.white,
          primaryColorDark: Color.fromRGBO(35, 35, 35, 1),
          accentColor: Color.fromRGBO(245, 193, 72, 1),
          fontFamily: "Roboto"),
      home: LoginScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  MainScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 2;

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  List<Widget> screens;
  @override
  void initState() {
    super.initState();
    screens = [
      SearchScreen(title: "ISKALNIK RASTLIN"),
      ProfileScreen(title: "PROFIL"),
      HomeScreen(),
      MyPlantsScreen(title: "MOJE RASTLINE"),
      DiaryScreen(title: "MOJ DNEVNIK")
    ];
  }

  void onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  AlignmentDirectional animationAlignment = AlignmentDirectional.bottomCenter;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          child: Container(
              child: screens[_selectedIndex], key: ValueKey(_selectedIndex))),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text(""),
            backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(""),
            backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt),
            title: Text(""),
            backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text(""),
            backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.import_contacts),
            title: Text(""),
            backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Theme.of(context).accentColor,
        onTap: onItemTapped,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}
