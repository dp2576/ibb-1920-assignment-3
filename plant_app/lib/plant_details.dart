library profile;

import 'package:flutter/material.dart';
import 'package:plant_app/plant_model.dart';

// A screen that allows users to take a picture using a given camera.
class PlantDetailsScreen extends StatefulWidget {
  PlantDetailsScreen({Key key, this.plant}) : super(key: key);
  final Plant plant;
  @override
  PlantDetailsScreenState createState() => PlantDetailsScreenState();
}

class PlantDetailsScreenState extends State<PlantDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  static const TextStyle labelStyle = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto",
      color: Color.fromRGBO(245, 193, 72, 1));

  static const TextStyle textStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      color: Colors.white);

  static const TextStyle nameStyle = TextStyle(
    fontSize: 30,
    fontWeight: FontWeight.bold,
    fontFamily: "Roboto",
    color: const Color.fromRGBO(35, 35, 35, 1),
  );

  static const TextStyle titleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "Roboto");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: new AppBar(
        backgroundColor: Theme.of(context).accentColor,
        title: Row(children: <Widget>[
          Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
              child: Icon(
                Icons.favorite,
                size: 28,
              )),
          Text(
            widget.plant.imeSLO,
            style: titleStyle,
          ),
        ]),
      ),
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 35.0, 0, 0),
                    child: Container(
                        height: 200.0,
                        width: 200.0,
                        child: Container(
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              boxShadow: [
                                new BoxShadow(
                                    color: Colors.black12,
                                    offset: new Offset(0.0, 8.0),
                                    spreadRadius: 3,
                                    blurRadius: 10)
                              ],
                              image: new DecorationImage(
                                image: AssetImage(widget.plant.imagePath != null
                                    ? widget.plant.imagePath
                                    : "assets/images/plants/placeholder.png"),
                                fit: BoxFit.cover,
                              )),
                        ))),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 20.0, 0, 11.0),
                    child: Text(widget.plant.imeSLO.toUpperCase(),
                        style: nameStyle)),
                ClipPath(
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Container(
                      width: double.infinity,
                      color: new Color.fromRGBO(35, 35, 35, 1),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20, 120, 20, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              "Slovensko ime:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.imeSLO,
                                  style: textStyle,
                                )),
                            Text(
                              "Latinsko ime:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.imeLATIN,
                                  style: textStyle,
                                )),
                            Text(
                              "Domovina:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.domovina,
                                  style: textStyle,
                                )),
                            Text(
                              "Oskrba:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.oskrba,
                                  style: textStyle,
                                )),
                            if (widget.plant.prezimovanje != null)
                              Text(
                                "Prezimovanje",
                                style: labelStyle,
                              ),
                            if (widget.plant.prezimovanje != null)
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                  child: Text(
                                    widget.plant.prezimovanje,
                                    style: textStyle,
                                  )),
                            if (widget.plant.obrezovanje != null)
                              Text(
                                "Obrezovanje:",
                                style: labelStyle,
                              ),
                            if (widget.plant.obrezovanje != null)
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                  child: Text(
                                    widget.plant.obrezovanje,
                                    style: textStyle,
                                  )),
                            Text(
                              "Razmnoževanje:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.razmnozevanje,
                                  style: textStyle,
                                )),
                            Text(
                              "Presajanje:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.presajanje,
                                  style: textStyle,
                                )),
                            Text(
                              "Dognojevanje:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.dognojevanje,
                                  style: textStyle,
                                )),
                            Text(
                              "Varstvo:",
                              style: labelStyle,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                child: Text(
                                  widget.plant.varstvo,
                                  style: textStyle,
                                )),
                            if (widget.plant.napake != null)
                              Text(
                                "Najpogostejše napake:",
                                style: labelStyle,
                              ),
                            if (widget.plant.napake != null)
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5.0, 0, 20.0),
                                  child: Text(
                                    widget.plant.napake,
                                    style: textStyle,
                                  )),
                          ],
                        ),
                      )),
                  clipper: TopClipper(),
                ),
              ],
            ),
          )),
    );
  }
}

class TopClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    double height = 80.0;
    path.moveTo(0.0, height);
    path.quadraticBezierTo(size.width / 2, 0, size.width, height);
    path.lineTo(size.width, height);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
