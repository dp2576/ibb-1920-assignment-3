library takepicture;

import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:plant_app/custom_icons_icons.dart';
import 'package:plant_app/image_crop.dart';

// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  int selectedIndex = 1;
  List<CameraDescription> cameras;
  CameraController controller;
  bool isReady = false;

  @override
  void initState() {
    super.initState();
    setupCameras();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    controller.dispose();
    super.dispose();
  }

  Future<void> setupCameras() async {
    try {
      // initialize cameras.
      cameras = await availableCameras();
      // initialize camera controllers.
      controller = new CameraController(cameras[0], ResolutionPreset.medium);
      await controller.initialize();
    } on CameraException catch (_) {
      // do something on error.
    }
    setState(() {
      isReady = true;
    });
  }

  void onItemTapped(int index) {
    selectedIndex = index;
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        takePicture();
        break;
      case 2:
        pickImage();
        break;
    }
  }

  void pickImage() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ImageCropScreen(image: imageFile),
      ),
    );
  }

  takePicture() async {
    try {
      // Ensure that the camera is initialized.
      if (!isReady) return;
      final path = join(
        // Store the picture in the temp directory.
        // Find the temp directory using the `path_provider` plugin.
        (await getTemporaryDirectory()).path,
        '${DateTime.now()}.png',
      );

      await controller.takePicture(path);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ImageCropScreen(image: File(path)),
        ),
      );
    } catch (e) {
      // If an error occurs, log the error to the console.
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (isReady
          ?
          // If the camera is ready, display the preview.
          CameraPreview(controller)
          :
          // Otherwise, display a loading indicator.
          Center(child: CircularProgressIndicator())),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.arrow_back),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(CustomIcons.record, size: 60.0),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.image),
            title: Text(""),
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Colors.white,
        onTap: onItemTapped,
      ),
    );
  }
}
