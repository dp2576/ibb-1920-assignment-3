import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:plant_app/detect_plant.dart';

class ImageCropScreen extends StatefulWidget {
  ImageCropScreen({Key key, this.image}) : super(key: key);
  final File image;
  @override
  ImageCropScreenState createState() => ImageCropScreenState();
}

class ImageCropScreenState extends State<ImageCropScreen> {
  int selectedIndex = 1;

  @override
  void initState() {
    super.initState();
  }

  void onItemTapped(index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        Navigator.pop(context);
        break;
      case 2:
        cropImage();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: widget.image != null ? Image.file(widget.image) : Container(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromRGBO(0, 0, 0, 1),
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.arrow_back),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.refresh),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.crop),
            title: Text(""),
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Colors.white,
        onTap: onItemTapped,
      ),
    );
  }

  Future<Null> cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: widget.image.path,
      cropStyle: CropStyle.circle,
      aspectRatioPresets: [],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: "Crop",
        toolbarColor: Theme.of(context).accentColor,
        toolbarWidgetColor: Theme.of(context).primaryColorDark,
        backgroundColor: Theme.of(context).primaryColor,
        activeControlsWidgetColor: Theme.of(context).accentColor,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: false,
        showCropGrid: false,
      ),
    );
    if (croppedFile != null)
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetectPlantScreen(image: croppedFile),
        ),
      );
  }
}
