library myplants;

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:plant_app/plant_list.dart';
import 'package:plant_app/plant_model.dart';

// A screen that allows users to take a picture using a given camera.
class MyPlantsScreen extends StatefulWidget {
  MyPlantsScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  MyPlantsScreenState createState() => MyPlantsScreenState();
}

class MyPlantsScreenState extends State<MyPlantsScreen> {
  PlantList myPlants;
  bool _loading = true;
  @override
  void initState() {
    super.initState();
    loadPlantList();
  }

  void loadPlantList() async {
    String plantsJson =
        await rootBundle.loadString("assets/opisi-rastlin.json");
    var plantsMap = json.decode(plantsJson);
    myPlants = new PlantList.fromJson(plantsMap);
    myPlants.sortByLatinName();
    setState(() {
      _loading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  static const TextStyle titleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "Roboto");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        appBar: new AppBar(
          backgroundColor: Theme.of(context).accentColor,
          title: Row(children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
                child: Icon(
                  Icons.favorite,
                  size: 28,
                )),
            Text(
              widget.title,
              style: titleStyle,
            ),
          ]),
        ),
        body: (_loading
            ? Center(child: CircularProgressIndicator())
            : Center(
                child: ListOfPlants(myPlants.plants),
              )));
  }
}
