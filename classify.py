import keras
from mobilenet_v2 import MobileNetv2
from PIL import Image
import numpy as np
import sys


def load_weights(weights_file):
    
    #initialize monilenet model
    model = MobileNetv2((224, 224, 3), 20) 
    
    #load the trained weights
    model.load_weights(weights_file)
    
    #compile the model
    model.compile(loss=keras.losses.categorical_crossentropy,
                optimizer=keras.optimizers.Adadelta(),
                metrics=['accuracy'])
    
    return model

def load_image(image_file):
    
    #load image from file
    np_image = Image.open(image_file)
    
    #convert
    np_image = np.array(np_image).astype('float32')/255
    
    #correct dims
    np_image = np.expand_dims(np_image, axis=0)

    return np_image


def predict(image, model):
    
    #class labels
    labels = ["Anthurium scherzerianum", "Aphelandra squarrosa", "Beaucarnea recurvata", "Begonia rex", "Chlorophytum comosum", "Clivia miniata",
"Codiaeum variegatum", "Cyclamen persicum", "Cymbidium hybridi", "Dracaena deremensi", "Dracaena marginata", "Euphorbia pulcherrima", "Ficus benjamini",
"Gerbera hybrid", "Kalanchoe blossfeldiana", "Monstera deliciosa", "Rhododendron simsii", "Sansevieria cylindric", "Schlumbergera hybrid", "Zantedeschia aethiopica"]
    
    #use the loaded model to predict
    out = model.predict(image)

    #get the prediction
    prediction = np.argmax(out, axis=1)[0]

    return {"class": labels[prediction]}

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('Usage: python3 classify.py "image_file_path" "weights_file_path"')
        print(sys.argv)
    else:
        image_file = str(sys.argv[1])
        weights_file = str(sys.argv[2])
        data = predict(load_image(image_file), load_weights(weights_file))
        
        import json
        with open('classification.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)