import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.activations import *
from tensorflow.keras.utils import CustomObjectScope

tflite_converter = tf.lite.TFLiteConverter.from_keras_model_file("model.h5", custom_objects={'relu6': tf.nn.relu6})
tflite_model = tflite_converter.convert()
open("tf_lite_model.tflite", "wb").write(tflite_model)
